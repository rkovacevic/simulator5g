ChowChow = require('oas3-chow-chow').default
let jsonref = require('json-schema-ref-parser');

function getPath(paths, uriPath) {
  for (let i = 0; i < paths.length; i++) {
    let pathRegex = paths[i].replace(/\{.+?\}/gi, '[A-Za-z0-9]+?') + '$';
    if (uriPath.indexOf('?') > -1) {
      uriPath = uriPath.substring(0, uriPath.indexOf('?'));
    }
    if (uriPath.match(pathRegex)) return paths[i];
  }
}

async function createValidator(yamlFile) {
  let api = await jsonref.dereference(yamlFile);
  let paths = [];
  for (path in api.paths) {
    paths.push(path);
    for (verb in api.paths[path]) {
      // callbacks
      if (api.paths[path][verb].callbacks != null) {
        for (callback in api.paths[path][verb].callbacks) {
          let callbackObj = api.paths[path][verb].callbacks[callback];
          let callbackVirtualPath = '/__callbacks/' + callback;
          api.paths[callbackVirtualPath] = callbackObj[Object.keys(callbackObj)[0]];
          paths.push(callbackVirtualPath);
        }
      }
    }
  }
  const chow = new ChowChow(api)
  return {
    validateRequest: function(req, callbackName) {
      let path;
      if (callbackName == null) {
        path = getPath(paths, req.route.path);
      } else {
        path = '/__callbacks/' + callbackName;
      }
      try {
        chow.validateRequest(path, {
          method: req.route.method,
          body: req.body,
          header: req.headers
        });
        return undefined;
      } catch(err) {
        let formattedError = {
          cause: err.message
        };
        if (err.meta.rawErrors != null) {
          formattedError.invalidParams = err.meta.rawErrors.map((e) => {
            return {
              param: e.path,
              reason: e.error
            };
          });
        }
        return formattedError;
      }
    },
    validateResponse: function(req, res, callbackName) {
      let path;
      if (callbackName == null) {
        path = getPath(paths, req.url);
      } else {
        path = '/__callbacks/' + callbackName;
      }
      try {
        let headers = res.headers;
        if (headers == null) {
          headers = {
            'content-type': req.headers['content-type']
          }
        }
        chow.validateResponse(path, {
          method: req.route.method,
          'status': res.statusCode,
          body: res.body,
          header: headers
        });
        return undefined;
      } catch(err) {
        let formattedError = {
          cause: err.message
        };
        if (err.meta.rawErrors != null) {
          formattedError.invalidParams = err.meta.rawErrors.map((e) => {
            return {
              param: e.path,
              reason: e.error
            };
          });
        }
        return formattedError;
      }
    }
  }
}

module.exports = createValidator;
