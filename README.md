# Simulator 5g

## Clone

git clone --recurse-submodules https://rkovacevic@bitbucket.org/rkovacevic/simulator5g.git

## Installation

1. Install Node.js
2. Install yarn
3. Git clone this repo
4. Install dependencies:
```console
yarn install
```

## Running

Run:
```console
./start
```

To change default host (0.0.0.0) and port (8080), set the 'SIM5G_HOST' and 'SIM5G_PORT' env variables, e.g.:
```console
SIM5G_HOST=127.0.0.1 SIM5G_PORT=1234 ./start
```

To disable TLS, set the SIM5G_FORCE_HTTP=true, e.g.:
```console
SIM5G_FORCE_HTTP=true SIM5G_HOST=127.0.0.1 SIM5G_PORT=1234 ./start
```

Output is in file:
```console
simulator.log
```

To stop the server, run:
```console
./stop
```
(Note: process id is in file '.pid')

## Special endpoints

GET /last     - returns the record of last request and response 

DELETE /last  - forget last recorder request and response

GET /         - general version info

## TLS decryption

In order to decrypt TLS traffic in Wireshark, you can either import 'keys/key.pem' into 'RSA keys list' in Wireshark, or import 'secrets.log' as 'Pre-Master Secrets log filename' in Wireshark. 

You also need to disable Diffie-Hellman ciphers on the client, for example if you use a Java client add ""ECDHE" and "ECDH" to "jdk.tls.disabledAlgorithms":

"<java_path>/conf/security/java.security"
jdk.tls.disabledAlgorithms=SSLv3, RC4, DES, MD5withRSA, DH keySize < 1024, \
    EC keySize < 224, 3DES_EDE_CBC, anon, NULL, ECDHE, ECDH

Simulator server disables these ciphers by default. 
