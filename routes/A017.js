module.exports = (route, log) => {

  route({
    method: 'get',
    path: '/nudr-dr/v1/policy-data/ues/385911234567/sm-data', 
    yaml: 'TS29519_Policy_Data.yaml',
    handler: (request, response) => {
      return response({
        statusCode: 200,
        body: {
          "smPolicySnssaiData" : {
            "1-1A752B" : {
              "snssai": {
                "sst": 1,
                "sd": "1A752B"
              },
              "smPolicyDnnData" : {
                "a017.apn.com" : {
                  "dnn": "a017.apn.com",
                  "subscCats" : ["subscCats"]
                }
              }
            }
          }
        }
      });
    }
  });

}
