module.exports = (route, log) => {

  route({
    method: 'post',
    path: '/385911234567/update', 
    yaml: 'TS29512_Npcf_SMPolicyControl.yaml',
    callbackName: 'SmPolicyUpdateNotification',
    handler: (request, response) => {
      return response({
        statusCode: 400,
        body:  {
          "error": {
            "type": "string",
          },
          "ruleReports": [
            {
              "ruleStatus": "INACTIVE",
              "pccRuleIds": Object.keys(request.body.smPolicyDecision.pccRules),
            }
          ]
        }
      });
    }
  });

}
