module.exports = (route, log) => {

  route({
    method: 'post',
    path: '/example/1', 
    handler: (request, response) => {
      log.info('Some custom message');
      return response({
        statusCode: 200,
        headers: {
          'some-header': 'test'
        },
        body: {
          attribute: 'hello world'
        }
      });
    }
  });

  route({
    method: 'post',
    path: '/example/:index', 
    handler: (request, response) => {
      if (request.params.index != 2) return response(null);
      log.info('This only handles index 2: %s', request.params.index);
      return response({
        statusCode: 200,
        headers: {
          'some-header': 'test'
        },
        body: {
          attribute: 'hello world from 2'
        }
      });
    }
  });

  route({
    method: 'post',
    path: '/example/:index', 
    handler: (request, response) => {
      log.info('This handles all other indexes: %s', request.params.index);
      return response({
        statusCode: 200,
        headers: {
          'some-header': 'test'
        },
        body: {
          attribute: ('hello world from ' + request.params.index)
        }
      });
    }
  });

  route({
    method: 'post',
    path: '/javascript', 
    handler: (request, response) => {
      log.info('2 minute intro to javascript');
      // This is a comment
      // variable declaration:
      let x = 1;
      // there is no type, all variables are declared with let:
      let some_string = 'Use quotes for strings';
      // arrays go like this:
      let some_array = [1, 2, 3];
      let or_string_array = ['one', 'two'];
      // objects are JSON:
      let some_object = {
        'attribute': 'value'
      };
      // access variables in object like this:
      let a = some_object.attribute;
      // or like this:
      a = some_object['attribute'];
      // for loop:
      for (let i = 0; i < some_array.length; i++) {
        log.info(some_array[i]);
      }
      // conditionals:
      if (x == 1) {
        log.info('x is 1');
      }
      // for string manipulation (substring, indexof, replace...), see:
      // https://www.w3schools.com/js/js_string_methods.asp
      return response({
        statusCode: 200,
        body: {
          attribute: ('hello js')
        }
      });
    }
  });

}
