var R = require('ramda');
var path = require('path');
var fs = require('fs');
var restify = require('restify');
var pino = require('pino');
var log = pino({
  prettyPrint: { colorize: false }
});
var routes = require('require-all')(__dirname + '/routes');
var appInfo = require('./package.json');
var validator = require('./validator');

var serverOptions = undefined;
if (process.env.SIM5G_FORCE_HTTP == null || process.env.SIM5G_FORCE_HTTP == 'false') {
  serverOptions = {
    http2: {
      cert: fs.readFileSync(path.join(__dirname, './keys/cert.pem')),
      key: fs.readFileSync(path.join(__dirname, './keys/key.pem')),
      ca: fs.readFileSync(path.join(__dirname, './keys/http2-csr.pem')),
      ciphers: [
          "AES128-GCM-SHA256",
          "RC4",
          "HIGH",
          "!DHE",
          "!ECDHE",
          "!MD5",
          "!aNULL"
        ].join(':')
    }
  }
}
var server = restify.createServer(serverOptions);

server.server.on('secureConnection', function(socket) {
  socket.on('keylog', function(line) {
    log.info(line)
    fs.appendFileSync('secrets.log', line);
  });
});
server.server.on('keylog', function(line) {
  fs.appendFileSync('secrets.log', line)
});

server.use(restify.plugins.bodyParser());

var lastRecorded = {};

server.use(function(req, res, next) {
  var serialized = pino.stdSerializers.req(req);
  serialized.body = req.body;
  serialized.params = req.params;
  log.info(serialized, 'Request received');
  return next();
});

server.on('restifyError', function(req, res, err, next) {
  var serialized = pino.stdSerializers.req(req);
  serialized.body = req.body;
  log.warn(serialized, 'Request received with no route');
  return next();
});

server.get('/', function(req, res, next) {
  res.send(appInfo);
  next(false);
});

server.get('/last', function(req, res, next) {
  if (lastRecorded.request != null) {
    res.send(lastRecorded);
  } else {
    res.send(404);
  }
  next(false);
});

server.del('/last', function(req, res, next) {
  lastRecorded = {};
  res.send(200);
  next(false);
});

let validators = {};
let routeMethods = {};
for (routeName in routes) {
  routes[routeName](function(routeDefinition) {
    if (routeDefinition.yaml != null) {
      validators[routeDefinition.yaml] = {};
    }
    routeDefinition._routeName = routeName;
    if (routeMethods[routeDefinition.method] == null) routeMethods[routeDefinition.method] = {};
    if (routeMethods[routeDefinition.method][routeDefinition.path] == null) routeMethods[routeDefinition.method][routeDefinition.path] = [];
    routeMethods[routeDefinition.method][routeDefinition.path].push(
      function(req, res, next) {
        // validate request
        if (routeDefinition.yaml != null) {
          let errors = validators[routeDefinition.yaml].validateRequest(req, routeDefinition.callbackName);
          if (errors != null) {
            // send validation error response
            res.send(400, errors);
            res._routeName = 'validator';
            return next(false);
          }
        }
        // handle request
        routeDefinition.handler(req, function(handlerResponse) {
          if (handlerResponse == null) return next();
          if (routeDefinition.yaml != null) {
            // validate response
            let errors = validators[routeDefinition.yaml].validateResponse(req, handlerResponse, routeDefinition.callbackName);
            if (errors != null) {
              // send validation error response
              res.send(500, errors);
              res._routeName = 'validator';
              return next(false);
            }
          }

          if (handlerResponse.headers !== null) {
            for (headerName in handlerResponse.headers) {
              res.header(headerName, handlerResponse.headers[headerName]);
            }
          }
          // send response
          res.send(handlerResponse.statusCode, handlerResponse.body);
          res._routeName = routeDefinition._routeName;
          return next(false);
        });
      }
    )
  }, 
  log.child({route: routeName}));
}

for (routeMethod in routeMethods) {
  for (routePath in routeMethods[routeMethod]) {
    server[routeMethod](routePath, ...routeMethods[routeMethod][routePath]);
  }
}

server.on('after', function(req, res, err) {
  var serializedReq = pino.stdSerializers.req(req);
  var serialized = pino.stdSerializers.res(res);
  serialized.id = serializedReq.id;
  serialized.body = res._body;
  if (req.route != null && req.route.path != '/last') {
    serializedReq.body = req.body;
    lastRecorded.request = R.clone(serializedReq);
    lastRecorded.response = R.clone(serialized);
  }
  log.info(serialized, 'Response sent by ' + res._routeName);
});

async function initValidators() {
  for (yaml in validators) {
    log.info('Initializing validator for: %s', yaml);
    validators[yaml] = await validator(path.join(__dirname, 'yaml', yaml));
  }
}

initValidators().then(function() {
  server.listen(process.env.SIM5G_PORT || 8080, process.env.SIM5G_HOST || '0.0.0.0', function() {
    log.info('Simulator running on %s', server.url);
  });
}, function(error) {
  log.error('Error initializing validators: %s', error);
});

